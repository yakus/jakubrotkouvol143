﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainCollision : MonoBehaviour {

    // Game Manager Reference
    public GameManager _gm;

    // Detecting when the launched ball has hit the ground to reset the camera and enable a new shot.
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Projectile")
        {
            _gm.Reset();
        }
    }
}
