﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingBehaviour : MonoBehaviour {

    public bool mouseDown = false;
    //Vector2 mousePosCurr;
    public GameManager _gm;
    public GameObject _pivot;
    public GameObject _launch;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    // Detects when a user clicks the sling with thir cursor
    // the cursor has to be over the sling's collider
    // The ethod notifies Game Manager that the user is pressing
    // the mousebutton
    void OnMouseDown()
    {
        mouseDown = true;
        _gm.SpawnGhostBall();
    }

    // Method notifies GameManager that the button has been lifted
    void OnMouseUp()
    {
        mouseDown = false;
    }

}
