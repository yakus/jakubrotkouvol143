﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGhostBehaviour : MonoBehaviour {

    // Memeber variables
    public SlingBehaviour _sling;
    public Camera _cam;
    public Transform _pivot;
    public float _radius;

	// Use this for initialization
	void Start () {
        _pivot = transform.parent;
        transform.position += Vector3.up * _radius;
	}
	
	// Update is called once per frame
    // In the update the angle indicator is allowed to move within a given radius
    // The indicator is allowed to follow the cursor to regulate the power of the shot
    // Once the button is lifted the projectile is spawned and the indicator destroyed.
	void Update () {

        // Track cursor position
        Vector2 pos = Input.mousePosition;

        // If the sling is being clicked and held:
        if (_sling.mouseDown == true)
        {
            Vector3 ballVector = _sling._gm._cam.WorldToScreenPoint(_pivot.position);
            ballVector = Input.mousePosition - ballVector;
            float angle = Mathf.Atan2(ballVector.y, ballVector.x) * Mathf.Rad2Deg;
            //print("Angle: " + (angle - 90));
            
            float clampedAngle = angle - 90;
            if (clampedAngle > 0 && clampedAngle < 90)
            {
                clampedAngle = -269.0f;
            }
            clampedAngle = Mathf.Clamp(clampedAngle, -269.0f, -200.0f);

            _pivot.rotation = Quaternion.AngleAxis(clampedAngle, Vector3.forward);
            _sling._launch.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            pos = _cam.ScreenToWorldPoint(pos);

            if (Vector2.Distance(Vector2.zero, pos) <= _radius)
            {
                transform.position = pos;
            }

        }
        // Once the user lifted the button: 
        else
        {
            LaunchBall(Vector2.Distance(Vector2.zero, transform.position), _radius, _sling._gm._maxForce, _sling._gm._minForce, _pivot.rotation);
            _sling.mouseDown = false;
            gameObject.SetActive(false);
        }
	}

    // Method responsible for instantiating the actual projectile. 
    // Launches the projectile by adding force to it at an angle dependent from the indicator.
    private void LaunchBall(float distance, float maxRadius, float maxForce, float minForce, Quaternion angle)
    {
        //print("D: " + distance);
        //print("F: " + MapForce(distance, 0, maxRadius, minForce, maxForce));

        GameObject projectile = Instantiate(Resources.Load("Projectile", typeof(GameObject)), _sling._launch.transform.position, _sling._launch.transform.rotation) as GameObject;
        Rigidbody2D rb2d = projectile.GetComponent<Rigidbody2D>();
        rb2d.AddForce(projectile.transform.right * MapForce(distance, 0, maxRadius, minForce, maxForce) * -1, ForceMode2D.Impulse);
        _sling._gm._cam.transform.parent = projectile.transform;
    }

    // Method responsible for mapping the pull distance for the sling
    // to the force with which the projectile will be launched.
    protected float MapForce(float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

}
