﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    [Header("GameObjectsScripts")]
    public SlingBehaviour _sling;
    public Camera _cam;

    [Header("Variables")]
    public float _maxForce;
    public float _minForce;
    public bool _canTakeShot = true;
    GameObject _ghostBall;

	// Use this for initialization
	void Start () {
        _ghostBall = Instantiate(Resources.Load("Ball-02", typeof(GameObject)), Vector2.zero, Quaternion.identity, _sling.transform.GetChild(0)) as GameObject;
        BallGhostBehaviour bgbTemp =_ghostBall.GetComponent<BallGhostBehaviour>();
        bgbTemp._sling = _sling.GetComponent<SlingBehaviour>();
        bgbTemp._cam = _cam;
        _ghostBall.SetActive(false);
    }

    // Creates the 'ghostball' that indicates the angle of shooting
    // and its power
    public void SpawnGhostBall()
    {
        _ghostBall.SetActive(true);
    }

    // Method used to fire up the reset coroutine
    public void Reset()
    {
        StartCoroutine(ReloadShot(1.0f));
    }

    // This coroutine counts down a second before reseting
    // the camera and enable shooting for the player.
    private IEnumerator ReloadShot(float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration)
        {
            yield return null;
        }
        _canTakeShot = true;
        _cam.transform.position = new Vector3(15, 0, -10);
        _cam.transform.parent = null;
    }
}
