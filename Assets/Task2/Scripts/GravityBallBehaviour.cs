﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityBallBehaviour : MonoBehaviour {

    public const double G = 6.67E-11;

    public GravityManager _gravMan;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach (GameObject gB in _gravMan._asteroids) {
            
        }
	}

    private float CalculateGravityAcceleration(float massOne, float massTwo, float radius)
    {
        return (float)(G * massOne * massTwo) / radius;
    }
}
